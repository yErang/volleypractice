package com.erang.volleyrecyclerview

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuInflater
import android.widget.Toast

import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    private val URL_JSON = "https://gist.githubusercontent.com/aws1994/f583d54e5af8e56173492d3f60dd5ebf/raw/c7796ba51d5a0d37fc756cf0fd14e54434c547bc/anime.json"
    private var ArrayRequest: JsonArrayRequest? = null
    private var requestQueue: RequestQueue? = null
    private val lstAnime = ArrayList<Anime>()
    private var myrv: RecyclerView? = null
    private var jsonObject: JSONObject? = null


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        myrv = findViewById(R.id.rv)
        jsoncall()


    }

    fun jsoncall() {


        ArrayRequest = JsonArrayRequest(URL_JSON, Response.Listener { response ->


            for (i in 0 until response.length()) {

                //Toast.makeText(getApplicationContext(),String.valueOf(i),Toast.LENGTH_SHORT).show();

                try {

                    jsonObject = response.getJSONObject(i)
                    val anime = Anime()
                    anime.name = jsonObject!!.getString("name")
                    anime.rating = jsonObject!!.getString("Rating")
                    anime.description = jsonObject!!.getString("description")
                    anime.image_url = jsonObject!!.getString("img")
                    anime.studio = jsonObject!!.getString("studio")
                    anime.categorie = jsonObject!!.getString("categorie")
                    lstAnime.add(anime)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

            }

            Toast.makeText(this@MainActivity, "Success", Toast.LENGTH_SHORT).show()

            setRvadapter(lstAnime)
        }, Response.ErrorListener { })


        requestQueue = Volley.newRequestQueue(this@MainActivity)
        requestQueue!!.add(ArrayRequest!!)
    }

    fun setRvadapter(lst: List<Anime>) {

        val myAdapter = RvAdapter(this, lst)
        myrv!!.layoutManager = LinearLayoutManager(this)
        myrv!!.adapter = myAdapter

    }

}