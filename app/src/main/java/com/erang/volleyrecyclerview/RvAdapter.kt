package com.erang.volleyrecyclerview

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by Aws on 28/01/2018.
 */

class RvAdapter(private val mContext: Context, lst: List<Anime>) : RecyclerView.Adapter<RvAdapter.MyViewHolder>() {

    internal var options: RequestOptions
    private val mData: List<Anime>


    init {
        this.mData = lst
        options = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view: View
        val mInflater = LayoutInflater.from(mContext)
        view = mInflater.inflate(R.layout.row, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.tvname.text = mData[position].name
        holder.tv_rate.text = mData[position].rating
        holder.tvstudio.text = mData[position].studio
        holder.tvcat.text = mData[position].categorie
        // load image from the internet using Glide
        Glide.with(mContext).load(mData[position].image_url).apply(options).into(holder.AnimeThumbnail)

    }

    override fun getItemCount(): Int {
        return mData.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var tvname: TextView= itemView.findViewById(R.id.rowname)
        internal var tv_rate: TextView = itemView.findViewById(R.id.rating)
        internal var tvstudio: TextView= itemView.findViewById(R.id.studio)
        internal var tvcat: TextView = itemView.findViewById(R.id.categorie)
        internal var AnimeThumbnail: ImageView = itemView.findViewById(R.id.thumbnail)

    }
}

